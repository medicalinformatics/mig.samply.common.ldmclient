/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient;

import java.util.Collection;
import java.util.Map;

public class LdmClientUtil {

  private LdmClientUtil() {

  }

  /**
   * Modifies a String, so that it ends with exactly one slash
   *
   * @param source the source string
   * @return the string, ending with a slash
   */
  public static String addTrailingSlash(String source) {
    if (source == null) {
      return "";
    }
    return source.replaceAll("/+$", "") + "/";
  }

  public static boolean isNullOrEmpty(final String s) {
    return s == null || s.isEmpty();
  }

  public static boolean isNullOrEmpty(final Collection<?> c) {
    return c == null || c.isEmpty();
  }

  public static boolean isNullOrEmpty(final Map<?, ?> m) {
    return m == null || m.isEmpty();
  }
}
