/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.common.ldmclient;

import de.samply.share.model.query.common.Error;
import de.samply.share.model.query.common.View;
import de.samply.share.model.queryresult.common.QueryResultStatistic;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpHead;
import org.apache.http.impl.client.CloseableHttpClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

/**
 * Abstract class to communicate with local datamanagement implementations
 */
public abstract class LdmClient<T> {

  private static final Logger logger = LoggerFactory.getLogger(LdmClient.class);

  private transient CloseableHttpClient httpClient;
  private String ldmBaseUrl;

  public LdmClient(CloseableHttpClient httpClient, String ldmBaseUrl) throws LdmClientException {
    if (httpClient == null) {
      throw new LdmClientException("No httpclient set");
    }
    if (LdmClientUtil.isNullOrEmpty(ldmBaseUrl)) {
      throw new LdmClientException("No LDM base URL provided");
    }
    this.httpClient = httpClient;
    this.ldmBaseUrl = ldmBaseUrl;
  }

  public CloseableHttpClient getHttpClient() {
    return httpClient;
  }

  public void setHttpClient(CloseableHttpClient httpClient) {
    this.httpClient = httpClient;
  }

  public String getLdmBaseUrl() {
    return ldmBaseUrl;
  }

  public void setLdmBaseUrl(String ldmBaseUrl) {
    this.ldmBaseUrl = ldmBaseUrl;
  }

  /**
   * Posts a view to local datamanagement and returns the location of the result.
   *
   * @param view a view (query + viewfields) object that is understood by the respective LDM. The
   * implementing class has to take care of serializing the view
   * @return the location of the result
   */
  public abstract String postView(View view) throws LdmClientException;

  /**
   * Posts an xml view to local datamanagement and returns the location of the result.
   *
   * @param viewString a serialized view (query + viewfields) that is understood by the respective
   * LDM
   * @return the location of the result
   */
  public abstract String postViewString(String viewString) throws LdmClientException;

  /**
   * Get the complete result that is found at the given location
   *
   * @param location The location (URL) where the result can be found
   * @return the query result.
   */
  public abstract T getResult(String location) throws LdmClientException;

  /**
   * Get the given page of the result that is found at the given location
   *
   * @param location The location (URL) where the result can be found
   * @param page the number of the result page to be retrieved
   * @return the query result page
   */
  public abstract T getResultPage(String location, int page) throws LdmClientException;

  /**
   * Get the object that is found at the given location under the resource /stats
   *
   * @param location The location (URL) where the statistics or error can be found
   * @return the query result statistics file if the request could be processed, an error object
   * otherwise
   */
  public abstract Object getStatsOrError(String location) throws LdmClientException;

  /**
   * Get the query result statistics file that is found at the given location under the resource
   * /stats
   *
   * @param location The location (URL) where the statistics can be found
   * @return the query result statistics file if the request could be processed
   */
  public abstract QueryResultStatistic getQueryResultStatistic(String location)
      throws LdmClientException;

  /**
   * Get the error file that is found at the given location under the resource /stats
   *
   * @param location The location (URL) where the error can be found
   * @return the error file
   */
  public abstract Error getError(String location) throws LdmClientException;

  /**
   * Get the amount of entities in the result
   *
   * @param location The location (URL) where the result can be found
   * @return the amount of entities (most likely patients) found
   */
  public abstract Integer getResultCount(String location) throws LdmClientException;

  /**
   * Get version information
   *
   * @return String representation of the Version Number
   */
  public abstract String getVersionString() throws LdmClientException;

  /**
   * Get a combination of LDM name and version number
   *
   * @return LDM Name and version number. Separated by a single slash.
   */
  public abstract String getUserAgentInfo() throws LdmClientException;

  /**
   * Check if the given location replies with 200 or not
   *
   * @param location where to check
   * @return true if 200 OK is received, otherwise reply false
   */
  public boolean isQueryPresent(String location) throws LdmClientException {
    if (LdmClientUtil.isNullOrEmpty(location)) {
      throw new LdmClientException("Location of query is empty");
    }
    try {
      URI uri = new URI(location);
      HttpHead httpHead = new HttpHead(uri.normalize().toString());

      CloseableHttpResponse response;
      int statusCode = 0;
      try {
        response = httpClient.execute(httpHead);
        statusCode = response.getStatusLine().getStatusCode();
        response.close();
      } catch (IOException e) {
        logger.error("Error trying to check for query presence", e);
      }

      return statusCode == HttpStatus.SC_OK;
    } catch (URISyntaxException e) {
      throw new LdmClientException(e);
    }
  }

  /**
   * Check if a given result page is available
   *
   * This is not implemented here (contrary to isQueryPresent()) because different LDMs might have
   * different structures in the REST path
   *
   * @param location where to check
   * @param pageIndex the page to try to get
   * @return true if 200 OK is returned, false otherwise and on errors
   */
  public abstract boolean isResultPageAvailable(String location, int pageIndex);

}
